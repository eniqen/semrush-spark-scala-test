scalaVersion := "2.11.7"

name := "semrush-spark-scala-test"

version := "1.0"

val sparkVersion = "1.4.0"
val json4sVersion = "3.2.11"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-streaming" % sparkVersion,
  "org.json4s" %% "json4s-jackson" % json4sVersion,
  "org.json4s" %% "json4s-ext" % json4sVersion,
  "org.scalatest" % "scalatest_2.11" % "2.2.6" % "test",
  "com.typesafe.scala-logging" %% "scala-logging-slf4j" % "2.1.2")




