package com.semrush.data

import java.util.Date

import com.semrush.serializer.EventSerializer
import org.apache.spark.streaming.Durations
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.json4s.jackson.Serialization
import org.json4s.jackson.Serialization.write

/**
  * Кейс класс для работы с событиями ClickHouse
  *
  * @param server_stamp     Unix timestamp в миллисекундах, когда событие было зарегистрировано на сервере.
  * @param client_stamp     Unix timestamp в миллисекундах, представляющий время у пользователя.
  * @param url              URL, по которому зашел пользователь.
  * @param domain           TLD сайта, полученный из url.
  * @param referer          URL, с которого перешел пользователь (может отсутствовать в случае прямого захода).
  * @param referer_domain   TLD сайта, полученный из referer (может отсутствовать в случае прямого захода).
  * @param status_code      Ответ от HTTP-сервера на запрошеный URL.
  * @param country_code     Двухбуквенный код страны (ISO 3166).
  * @param browser          Браузер пользователя.
  * @param operating_system Операционная система пользователя.
  * @param uid              Уникальный идентификатор пользователя.
  * @param sid              Идентификатор сессии
  */
case class Event(server_stamp: Date,
                 client_stamp: Date,
                 url: String,
                 domain: String,
                 referer: Option[String],
                 referer_domain: Option[String],
                 status_code: Int,
                 country_code: String,
                 browser: String,
                 operating_system: String,
                 uid: String,
                 sid: String
                ) extends Serializable {

  /**
    * Проверка текущий класс является родительским по отношению к ребенку и можем быть как сессией так и чилдом имеющим sid такой же как у сессии
    *
    * @param that следующий евент
    * @tparam E тип евента
    * @return результат проверки
    */
  def isSessionOrParentFor[E <: Event](that: E): Boolean = {
    (this.uid == that.uid
      && this.domain == that.domain
      && Durations.minutes(30L).greater(Durations.milliseconds(that.client_stamp.getTime - this.client_stamp.getTime))
      && ((that.referer_domain.nonEmpty && that.referer_domain.get == this.domain) || that.referer_domain.isEmpty))
  }
}

/**
  * Компаниан объект сущности события
  */
object Event {
  implicit val formats = Serialization.formats(NoTypeHints) + new EventSerializer
  /**
    * Получение идентификатора сессии
    *
    * @param uid        уникальный идентификатор клиента
    * @param domain     домен сайта
    * @param startStamp время старта сессии клиента
    * @return идентификатор сессии
    */
  def makeSid(uid: String, domain: String, startStamp: java.util.Date): String = {
    startStamp.getTime.toString + (uid, domain).hashCode
  }

  /**
    * Маппим строчку c евентом в кейс класс
    *
    * @param eventJson строка с евенто
    * @return результат дессериализации
    */
  def fromJson(eventJson: String): Event = {
    parse(eventJson).extract[Event]
  }

  /**
    * Маппим кейс класс в строчку
    *
    * @param event сущность событий
    * @return строка с json eвента
    */
  def toJson(event: Event): String = {
    write(event)
  }
}
