package com.semrush.data

import org.apache.spark.rdd.RDD

/**
  * Процессор реализующий логику вычисления сессии
  */
object DataProcessor {

  def apply(rawRDD: RDD[String]): RDD[Event] = {
    rawRDD.flatMap(_.split("\n"))
      .map(Event.fromJson)
      .sortBy(_.client_stamp)
      .groupBy(e => (e.uid, e.domain))
      .mapValues(_.foldLeft(List[Event]()) {
        (accum, event) =>
          val e = accum match {
            case head :: Nil => if (head.isSessionOrParentFor(event)) event.copy(sid = head.sid) else event
            case head :: tail => if (tail.last.isSessionOrParentFor(event)) event.copy(sid = tail.last.sid) else event
            case _ => event
          }
          accum :+ e
      }).flatMap(_._2)
  }
}
