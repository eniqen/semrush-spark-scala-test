package com.semrush.serializer

import java.sql.Timestamp

import com.semrush.data.Event
import org.json4s.JsonDSL._
import org.json4s.{CustomSerializer, JObject}

/**
  * Кастомный сериализатор для евентов {@link com.semrush.test.Event}
  */
class EventSerializer extends CustomSerializer[Event](implicit format => ( {
  case jsonEvent: JObject =>
    val server_stamp = (jsonEvent \ "server_stamp").extract[Long]
    val client_stamp = (jsonEvent \ "client_stamp").extract[Long]
    val url = (jsonEvent \ "url").extract[String]
    val domain = (jsonEvent \ "domain").extract[String]
    val referer = (jsonEvent \ "referer").extractOpt[String]
    val referer_domain = (jsonEvent \ "referer_domain").extractOpt[String]
    val status_code = (jsonEvent \ "status_code").extract[Int]
    val country_code = (jsonEvent \ "country_code").extract[String]
    val browser = (jsonEvent \ "browser").extract[String]
    val operating_system = (jsonEvent \ "operating_system").extract[String]
    val uid = (jsonEvent \ "uid").extract[String]

    Event(
      new Timestamp(server_stamp),
      new Timestamp(client_stamp),
      url,
      domain,
      referer,
      referer_domain,
      status_code.toInt,
      country_code,
      browser,
      operating_system,
      uid,
      Event.makeSid(uid, domain, new Timestamp(client_stamp))
    )
}, {
  case e: Event =>
    ("name" -> e.server_stamp.getTime) ~
      ("client_stamp" -> e.client_stamp.getTime) ~
      ("url" -> e.url) ~
      ("domain" -> e.domain) ~
      ("referer" -> e.referer) ~
      ("referer_domain" -> e.referer_domain) ~
      ("status_code" -> e.status_code) ~
      ("country_code" -> e.country_code) ~
      ("browser" -> e.browser) ~
      ("operating_system" -> e.operating_system) ~
      ("uid" -> e.uid)
}))
