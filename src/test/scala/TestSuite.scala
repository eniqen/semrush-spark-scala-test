
import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Вспомогательный теск класс для работы с тестовым спарк контекстом
  */
object TestSuite {
  //==================================================================================
  {
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)
  }
  //==================================================================================

  val conf = new SparkConf().setAppName("scalyzer_test_suite").setMaster("local[*]")
  val sc = new SparkContext(conf)
}
